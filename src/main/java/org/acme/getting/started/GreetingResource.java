package org.acme.getting.started;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class GreetingResource {

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String hello() {
        String style = "<style type='text/css' media='screen'>";
        style += "body { background-color: purple; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); color: black; font-size: 250%; }";
        style += "</style>";
        
        String message = "Greetings from GitLab!";
        
        String body = "<body>" + message + "</body>";

        return style + body;

    }
}